import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

System.setProperty("geb.build.reportsDir", "target/get-reports")
System.setProperty("webdriver.chrome.driver", "./browserDrivers/chromedriver83.exe")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com/")
System.setProperty("autoqa.remoteUrl", "http://192.168.0.101:4446/wd/hub")

hubUrl = new URL(System.getProperty("autoqa.remoteUrl"))

driver = {
    ChromeOptions options = new ChromeOptions()
    options.addArguments("--start-maximized")
//    DesiredCapabilities capabilities = DesiredCapabilities.chrome()
//    capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT)
//    capabilities.setCapability(ChromeOptions.CAPABILITY, options)
    def driver = new ChromeDriver(options)
    return driver
}

environments{
    'remote-chrome' {
        driver = {
            ChromeOptions options = new ChromeOptions()
            DesiredCapabilities capabilities = DesiredCapabilities.chrome()
            capabilities.setCapability(ChromeOptions.CAPABILITY, options)
            def driver = new RemoteWebDriver(hubUrl, capabilities)
            return driver

        }
    }
}

