package nix.auto.framework.spec

import geb.spock.GebReportingSpec
import nix.auto.framework.page.*

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"() {
        when:
            to StartPage
            "User clicks the Blog navigation"()
        then:
            at BlogPage

    }

}
