package nix.auto.framework.page

import geb.Page

class StartPage extends Page {

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    static content = {
        BlogLink (wait:true) {$($("a", text:"Blog"), $("a", text:"Блог"))}
    }

    def "User clicks the Blog navigation"(){
        waitFor(){BlogLink.isDisplayed()}
        BlogLink.click()
    }

}

